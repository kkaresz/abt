from setuptools import setup

setup(name='abt',
      version='0.1',
      description='Android Build Tool',
      long_description='Uber home assigment: Android Build Tool',
      classifiers=[
      	'Development Status :: 3 - Alpha',
      	'License :: OSI Approved :: GNU General Public License v3',
      	'Programming Language :: Python :: 2.7',
      	'Topic :: Software :: Build Tool',
      ],
      url='https://bitbucket.org/kkaresz/abt',
      author='Karoly Kamaras',
      author_email='karoly.kamaras@gmail.com',
      keywords='uber android build',
      license='GPLv3',
      packages=['abt'],
      install_requires=[
            'setuptools',
            'configparser'
      ],
      entry_points={
      	'console_scripts': ['abt = abt.cli:main'],
      	},
      include_package_data=True,
      zip_safe=True)