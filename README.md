# README #

**abt** as in Android Build Tool (for Uber home assignment)

## Disclaimer
The content of this repository is a command line tool which can create, compile, package and launch an Android application using the Android SDK command line tools and a simple python configuration file. To be able to use the full functionality of the tool you need to have Java JDK (preferably 1.7) and the Android SDK installed, `ANDROID_HOME` and `JAVA_HOME` env variables set, and have Android virtual devices installed, so you can launch your app to an emulator. 


----------


## Requirements
- Java SDK 1.7 installed and `JAVA_HOME` env var set
- Android SDK installed and `ANDROID_HOME` env var set
- AVD set up with a few ABIs are installed (to be able to run your app on an emulator)
- Python 2.7 and `virtualenv` python package (version >= 13) installed

----------


## Installation
```sh
git clone https://bitbucket.org/kkaresz/abt
cd abt
virtualenv virtualenv # optional
. virtualenv/bin/activate # optional
pip install -r requirements.txt
pip install -e .
abt -h
```
You don't need to install it in a virtualenv, but it's highly recommended so you don't ruing your system python packages.


----------

## Development
The tool is completely written in python, you can extend it with additional `tasks` in the tasks folder and config/android commands in the `lib` folder. After setting up the virtual environment you car start using and testing the tool. To run python unit tests run `python -m unittest discover` in the root directory of the project. 


----------

## Usage
- `abt -h` prints the default help with the available commands.
- `abt [command] -h` prints the help for the specific command.
- `abt [command] [parameters]` runs the speified command with the given parameters (if needed) and logs every output into standard out.


### Commands
- create
- compile
- package
- launch

#### Create 
Creates the skeleton structure and basic files for an Android project using the Android SDK tools. A config file called `abt.config` will be placed in the root folder of the Android application, where you can set additional attributes of the project. You can read more about the config file under _Configuration_ section below.

#### Parameters
- `-name` The name of your Android app
- `-target` The target Android SDK that you want to build your app on (run `$ANDROID_HOME/tools/android list targets` to see available targets on your system) 
- `-path` Path to the folder where the project will be created 
- `-package` Default Java package name of the application 
- `-activity` Default Android activity of the app
#### Example: 
```sh
$ abt create -name TestApp -target android-7 -path ~/workspace/testapp -package com.testapp.tespackage -activity testactivity
```

### Compile
Compiles the java files in your applications directory. You need to change your working directory to a previously created Android project folder to be able to run this command. Note that you need to be in an active virtualenv in case you didn't install the tool into your system python packages.

#### Parameters
No parameters for this command.
#### Example: 
```sh
$ cd ~/workspace/testapp
$ abt compile
```

### Package
Packages the compiled source code into an Android apk. First an unsigned apk is created, then the tool signs it with a provided debug keystore, finally it zip-aligns the apk.
#### Parameters
You need to pass the attributes of your debug keystore (keystore alias, storepass, keypass) either via command line parameters, or in the `abt.config` file (more about that in the _Configuration_ section below).
- `-keystore-path` Path to keystore file to sign the app with.
- `-keystore-alias` Keystore alias to sign the app with.
- `-keystore-storepass` Storepass of the keystore.
- `-keystore-keypass` Keypass of the keystore
#### Example:
```sh
$ abt package -keystore-keypass password -keystore-storepass password -keystore-alias AndroidTest -keystore-path ~/keys/AndroidTest.keystore
```

### Launch
Installs the packages and signed apk on a running emulator. You need to have a running emulator to be able to run this command. You can see what emulators you have available on your system by running `$ANDROID_HOME/tools/emulator -list-avds` and you can start one by running e.g. `$ANDROID_HOME/tools/emulator -wipe-data -avd Nexus4`.
#### Parameters
- `-device` _OPTIONAL_ Device ID of the emulator to intall the app on. If this parameter is not defined, the command will install the app on the only running emulator on your system.
#### Example
```sh 
abt launch -device emulator-5554
```
or
```sh
abt launch
```

----------


## Configuration
**abt** uses a simply python configuration file (`.INI`) to make the the build process customizable. There are several sections in the file that can be filled with your custom build preferences. You can see a fully prepared config file in `abt.config.example` in the root of this repository.

### Config sections
```sh
[project]
name = # The name of your Android project.
target = #The target Android SDK that you want to build your app on.
package = #Default Java package name of the application
activity = #Default Android activity of the app

# All lines below are optional, you don't have to set any of these if you have at least 1 build too under$ANDOIRD_HOME/build-tools/
[android]
build_tool_version = # The versoin of your Android build-tool (a directory under $ANDOIRD_HOME/build-tools/) The tool automatically selects a build tool from $ANDOIRD_HOME/build-tools/ if this item is not set.
keystore_path = # Path to keystore file to sign the app with.
keystore_alias = # Keystore alias to sign the app with.
keystore_storepass = # Storepass of the keystore.
keystore_keypass = # Keypass of the keystore

[task_create]
before = # OPTIONAL A shell command that you would like to run before creating the project.
after = # A shell command that you would like to run after creating the project.

[task_compile]
before = # A shell command that you would like to run before compiling the project.
after = # A shell command that you would like to run after compiling the project.

[task_package]
before = # A shell command that you would like to run before packaging the app.
after = # A shell command that you would like to run after packaging the app.

[task_launch]
before = # A shell command that you would like to run before installing the app on an emulator.
after = # A shell command that you would like to run after installing the app on an emulator.
```
