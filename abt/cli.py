'''
Command line tool for building Android projects.
'''

import argparse

from abt.tasks.create import CreateTask
from abt.tasks.compile import CompileTask
from abt.tasks.package import PackageTask
from abt.tasks.launch import LaunchTask

from abt.lib import android_handler
from abt.lib.logger import Logger


def init_parser():
	parser = argparse.ArgumentParser(description='Command line tool for building Android projects.')
	subparsers = parser.add_subparsers(title="action")
	
	# create project attributes
	parser_create = subparsers.add_parser("create", help="create Android project")
	parser_create.add_argument("-name", help="Android project name.", required=True)
	parser_create.add_argument("-target", help="Target ID of the new project.", required=True)
	parser_create.add_argument("-path", help="The new project's directory.", required=True)
	parser_create.add_argument("-package", help="Android package name for the application.", required=True)
	parser_create.add_argument("-activity", help="Name of the default Activity that is created.", required=True)
	parser_create.set_defaults(func=create_project)

	# compile project attributes
	parser_compile = subparsers.add_parser("compile", help="compile project")
	parser_compile.set_defaults(func=compile)

	# package attributes
	parser_package = subparsers.add_parser("package", help="package project")
	parser_package.add_argument("-keystore-path", help="Path to keystore file to sign the app with.")
	parser_package.add_argument("-keystore-alias", help="Keystore alias to sign the app with.")
	parser_package.add_argument("-keystore-storepass", help="Storepass of the keystore.")
	parser_package.add_argument("-keystore-keypass", help="Keypass of the keystore")
	parser_package.set_defaults(func=package)

	# launch attributes
	parser_launch = subparsers.add_parser("launch", help="launch project")
	parser_launch.add_argument("-device", help="Device ID of the emulator to intall the app on.")
	parser_launch.set_defaults(func=launch)

	return parser


def create_project(args):
	Logger.info("Creating Android project: %s" % args.name)
	task = CreateTask()
	task.run(args)


def compile(args):
	Logger.info("Compile project")
	task = CompileTask()
	task.run(args)


def package(args):
	Logger.info("Package project")
	task = PackageTask()
	task.run(args)


def launch(args):
	Logger.info("Launch project")
	task = LaunchTask()
	task.run(args)


def main(args=None):
	Logger.init_logger()
	
	parser = init_parser()
	args = parser.parse_args(args)

	if not hasattr(args, "func"):
		parser.print_usage()
		parser.exit(1)

	return args.func(args)
