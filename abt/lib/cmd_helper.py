from abt.lib.logger import Logger

import subprocess


def run_shell_command(args, shell=False):
	Logger.info("Running command: %s" % args)
	proc = subprocess.Popen(args, shell=shell, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
	stdout, stderr = proc.communicate()
	retcode = proc.returncode
	if stderr:
		Logger.error(stderr)
	Logger.info(stdout)
	return retcode, stdout
