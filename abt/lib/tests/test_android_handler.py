import unittest

from mock import patch, call

from test.test_support import EnvironmentVarGuard
from unit_test_case_base import UnitTestCaseBase

from abt.lib.Config import Config
from abt.lib.android_handler import AndroidHandler


class AndroidHandlerTestCase(UnitTestCaseBase):
    
    env = None
    android = None

    def setUp(self):
        super(AndroidHandlerTestCase, self).setUp()
        
        self.env = EnvironmentVarGuard()
        self.env.set('ANDROID_HOME', '/my/android/home')
        self.env.set('JAVA_HOME', '/my/java/home')
        
        Config.inject_config('  [android]\n \
                                build_tool_version = 3.2.1\n \
                                keystore_path = test_ks_path\n \
                                keystore_alias = test_ks_alias\n \
                                keystore_keypass = test_ks_keypass\n \
                                keystore_storepass = test_ks_storepass\n \
                                [project]\n \
                                name=testName\n \
                                target=testTarget \
                                \npath=testPath \
                                \npackage=testPackage \
                                \nactivity=testActivity')

    def tearDown(self):
        super(AndroidHandlerTestCase, self).tearDown()
        pass
    
    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    def test_load_config_loads_env_vars_and_build_tool_version(self, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.load_config()
            self.assertEqual(self.android.ANDROID_HOME, '/my/android/home')
            self.assertEqual(self.android.JAVA_HOME, '/my/java/home')
            self.assertEqual(self.android.BUILD_TOOL_VERSION, '3.2.1')

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_list_targets_calls_android_with_proper_arguments(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.list_targets()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/tools/android", 
                "list", 
                "target", 
                "-c"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_create_project_calls_with_proper_arguments(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.create_project("testPath")
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/tools/android",
                "create", 
                "project",
                "--name", "testName",
                "--target", "testTarget",
                "--path", "testPath",
                "--package", "testPackage",
                "--activity", "testActivity"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_create_output_dirs_creates_the_proper_folders(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.create_output_dirs()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "mkdir",
                "-p",
                "bin/res",
                "bin/classes",
                "bin/dexedLibs",
                "gen"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_create_r_java_calls_with_proper_arguments(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.create_r_java()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/build-tools/3.2.1/aapt",
                "package",
                "-v",
                "-f",
                "-m",
                "-0", "apk",
                "-M", "AndroidManifest.xml",
                "-S", "bin/res",
                "-S", "res",
                "-I", "/my/android/home/platforms/testTarget/android.jar",
                "-J", "gen",
                "--generate-dependencies"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    @patch('abt.lib.android_handler.os.walk')
    def test_compile_calls_with_proper_arguments(self, mock_walk, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.compile()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/java/home/bin/javac",
                "-source", "1.7",
                "-target", "1.7",
                "-verbose",
                "-g",
                "-d", "bin/classes",
                "-classpath", "/my/android/home/platforms/testTarget/android.jar:bin/classes",
                "-sourcepath", "src:gen",
                "-bootclasspath", "/my/android/home/platforms/android-7/android.jar",
                "-encoding", "UTF-8"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_create_dex_file_calls_with_proper_arguments(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.create_dex_file()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/build-tools/3.2.1/dx",
                "--dex",
                "--verbose",
                "--output", "bin/classes.dex", "bin/classes", "bin/dexedLibs"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_package_to_apk_calls_with_proper_arguments(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.package_to_apk()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/build-tools/3.2.1/aapt",
                "package",
                "-v",
                "-f",
                "-M", "AndroidManifest.xml",
                "-S", "bin/res",
                "-S", "res",
                "-I", "/my/android/home/platforms/testTarget/android.jar",
                "-F", "bin/testName.unsigned.apk",
                "bin"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_sign_apk_calls_with_proper_arguments(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.sign_apk()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/java/home/bin/jarsigner",
                "-verbose",
                "-keystore", "test_ks_path",
                "-storepass", "test_ks_storepass",
                "-keypass", "test_ks_keypass",
                "-signedjar", "bin/testName.signed.apk",
                "bin/testName.unsigned.apk",
                "test_ks_alias"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_zip_align_apk_calls_with_proper_arguments(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.zip_align_apk()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/build-tools/3.2.1/zipalign",
                "-v",
                "-f", "4",
                "bin/testName.signed.apk",
                "bin/testName-release.apk"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_launch_project_calls_with_proper_arguments_if_no_device_is_set(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.launch_project()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/platform-tools/adb",
                "-e", 
                "install",
                "bin/testName-release.apk"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_launch_project_calls_with_proper_arguments_if_device_is_set(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.launch_project("test_device_id")
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/platform-tools/adb",
                "-s", "test_device_id", 
                "install",
                "bin/testName-release.apk"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)

    @patch('abt.lib.android_handler.AndroidHandler.build_tool_exists', return_value=True)
    @patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
    def test_launch_project_calls_with_proper_arguments_if_device_is_set(self, mock_run_shell_command, mock_build_tool_exists):
        with self.env:
            self.android = AndroidHandler()
            self.android.get_emulators()
            
            mocked_call_list = mock_run_shell_command.call_args_list[0]
            expected_call_list = call([
                "/my/android/home/platform-tools/adb",
                "devices"
            ])

            self.assertEqual(mocked_call_list, expected_call_list)
