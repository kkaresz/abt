import unittest
import os

from mock import patch, call

from test.test_support import EnvironmentVarGuard
from unit_test_case_base import UnitTestCaseBase

from abt.lib.Config import Config


class ConfigTestCase(UnitTestCaseBase):
    
    env = None
    android = None
    current_dir = None

    TEST_CONFIG_FILE = None
    NEW_TEST_CONFIG_FILE = None
    WRONG_TEST_CONFIG_FILE = None

    def setUp(self):
        super(ConfigTestCase, self).setUp()
        
        self.env = EnvironmentVarGuard()
        self.env.set('ANDROID_HOME', '/my/android/home')
        self.env.set('JAVA_HOME', '/my/java/home')

        self.TEST_CONFIG_FILE = "%s/test_config_files/test.config" % self.current_dir
        self.NEW_TEST_CONFIG_FILE = "%s/test_config_files/test.config.new" % self.current_dir
        self.WRONG_TEST_CONFIG_FILE = "%s/test_config_files/test.config.does_not_exist" % self.current_dir

        Config.clear_config()
        Config.CONFIG_FILE = self.TEST_CONFIG_FILE

    def tearDown(self):
        super(ConfigTestCase, self).tearDown()

        if os.path.isfile(self.NEW_TEST_CONFIG_FILE):
            os.remove(self.NEW_TEST_CONFIG_FILE)
        
    def test_load_config_from_file(self):
        Config.load_config()
        self.assertEqual(Config.get("android", "build_tool_version"), "3.2.1")
        self.assertEqual(Config.get("android", "keystore_path"), "test_ks_path")
        self.assertEqual(Config.get("android", "keystore_alias"), "test_ks_alias")
        self.assertEqual(Config.get("android", "keystore_keypass"), "test_ks_keypass")
        self.assertEqual(Config.get("android", "keystore_storepass"), "test_ks_storepass")
        self.assertEqual(Config.get("project", "name"), "testName")
        self.assertEqual(Config.get("project", "target"), "testTarget")
        self.assertEqual(Config.get("project", "package"), "testPackage")
        self.assertEqual(Config.get("project", "activity"), "testActivity")

    def test_get_config_item(self):
        Config.inject_config('[android]\n build_tool_version = 3.2.1\n')
        self.assertEqual(Config.get("android", "build_tool_version"), "3.2.1")

    def test_inject_config(self):
        Config.inject_config('  [android]\n \
                                build_tool_version = 3.2.1\n \
                                keystore_path = test_ks_path\n \
                                keystore_alias = test_ks_alias\n \
                                keystore_keypass = test_ks_keypass\n \
                                keystore_storepass = test_ks_storepass\n \
                                [project]\n \
                                name=testName\n \
                                target=testTarget \
                                \npath=testPath \
                                \npackage=testPackage \
                                \nactivity=testActivity')

        self.assertEqual(Config.get("android", "build_tool_version"), "3.2.1")
        self.assertEqual(Config.get("android", "keystore_path"), "test_ks_path")
        self.assertEqual(Config.get("android", "keystore_alias"), "test_ks_alias")
        self.assertEqual(Config.get("android", "keystore_keypass"), "test_ks_keypass")
        self.assertEqual(Config.get("android", "keystore_storepass"), "test_ks_storepass")
        self.assertEqual(Config.get("project", "name"), "testName")
        self.assertEqual(Config.get("project", "target"), "testTarget")
        self.assertEqual(Config.get("project", "package"), "testPackage")
        self.assertEqual(Config.get("project", "activity"), "testActivity")

    def test_set_config_item(self):
        Config.set("project", "name", "anotherTestName")
        self.assertEqual(Config.get("project", "name"), "anotherTestName")

    def test_set_config_item_if_section_does_not_exist(self):
        Config.set("new_section", "item", "newSectionItem")
        self.assertEqual(Config.get("new_section", "item"), "newSectionItem")

    @patch('abt.lib.logger.Logger.error')
    def test_config_file_exists(self, mock_error):
        Config.CONFIG_FILE = self.TEST_CONFIG_FILE
        self.assertTrue(Config.config_file_exists())

        Config.CONFIG_FILE = self.WRONG_TEST_CONFIG_FILE
        self.assertFalse(Config.config_file_exists())

    @patch('abt.lib.logger.Logger.error')
    def test_write_to_file(self, mock_error):
        Config.load_config()
        Config.CONFIG_FILE = self.NEW_TEST_CONFIG_FILE
        Config.write_to_file()

        self.assertTrue(os.path.isfile(self.NEW_TEST_CONFIG_FILE))
        Config.clear_config()
        Config.load_config()
        self.assertEqual(Config.get("android", "build_tool_version"), "3.2.1")
        self.assertEqual(Config.get("android", "keystore_path"), "test_ks_path")
        self.assertEqual(Config.get("android", "keystore_alias"), "test_ks_alias")
        self.assertEqual(Config.get("android", "keystore_keypass"), "test_ks_keypass")
        self.assertEqual(Config.get("android", "keystore_storepass"), "test_ks_storepass")
        self.assertEqual(Config.get("project", "name"), "testName")
        self.assertEqual(Config.get("project", "target"), "testTarget")
        self.assertEqual(Config.get("project", "package"), "testPackage")
        self.assertEqual(Config.get("project", "activity"), "testActivity")

    @patch('abt.lib.logger.Logger.error')
    def test_write_to_file_with_parameter(self, mock_error):
        Config.load_config()
        Config.write_to_file(self.NEW_TEST_CONFIG_FILE)
        
        self.assertTrue(os.path.isfile(self.NEW_TEST_CONFIG_FILE))
        self.assertEqual(Config.get("android", "build_tool_version"), "3.2.1")
        self.assertEqual(Config.get("android", "keystore_path"), "test_ks_path")
        self.assertEqual(Config.get("android", "keystore_alias"), "test_ks_alias")
        self.assertEqual(Config.get("android", "keystore_keypass"), "test_ks_keypass")
        self.assertEqual(Config.get("android", "keystore_storepass"), "test_ks_storepass")
        self.assertEqual(Config.get("project", "name"), "testName")
        self.assertEqual(Config.get("project", "target"), "testTarget")
        self.assertEqual(Config.get("project", "package"), "testPackage")
        self.assertEqual(Config.get("project", "activity"), "testActivity")
