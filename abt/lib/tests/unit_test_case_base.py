import unittest
import os


class UnitTestCaseBase(unittest.TestCase):
	
	current_dir = None

	def setUp(self):
		self.current_dir = os.path.dirname(os.path.realpath(__file__))
		pass
	
	def tearDown(self):
		pass
