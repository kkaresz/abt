import os
import StringIO

from configparser import ConfigParser
from abt.lib.logger import Logger


class Config(object):
	config = None
	CONFIG_FILE = "abt.config"

	@classmethod
	def load_config(cls):
		if not cls.config:
			if cls.config_file_exists():
				cls.config = ConfigParser()
				cls.config.read(cls.CONFIG_FILE)
		return cls.config

	@classmethod
	def inject_config(cls, cfg):
		cls.config = ConfigParser()
		cls.config.readfp(StringIO.StringIO(cfg))

	@classmethod
	def config_file_exists(cls):
		if not os.path.isfile(cls.CONFIG_FILE):
			Logger.error("[Config] No config file found called 'abt.config'.")
			return False
		return True

	@classmethod
	def create_config(cls):
		if not cls.config:
			cls.config = ConfigParser()
		return cls.config

	@classmethod
	def get(cls, section, key):
		if cls.config is None:
			cls.load_config()
		try:
			val = cls.config.get(section, key)
		except:
			return None
		return val

	@classmethod
	def set(cls, section, key, value):
		if cls.config is None:
			cls.load_config()
		try:
			if not cls.config.has_section(section):
				cls.add_section(section)
			cls.config.set(section, key, value)
		except:
			return False
		return True

	@classmethod
	def add_section(cls, section):
		if cls.config is None:
			cls.load_config()
		try:
			cls.config.add_section(section)
		except:
			return False
		return True

	@classmethod
	def print_config(cls):
		if cls.config is None:
			cls.load_config()
		sections = cls.config.sections()
		for section in sections:
			options = cls.config.options(section)
			for option in options:
				Logger.info("[Config] %s/%s: %s" % (section, option, cls.config.get(section, option)))

	@classmethod
	def write_to_file(cls, path=None):
		if cls.config is None:
			cls.load_config()
		try:
			config_file_path = path or cls.CONFIG_FILE
			config_file = open(config_file_path, 'w')
			cls.config.write(config_file)
			config_file.close()
		except IOError as e:
			Logger.error("[Config] Error while writing config file: %s" % e)
			raise IOError

	@classmethod
	def clear_config(cls):
		if cls.config is not None:
			cls.config = None
