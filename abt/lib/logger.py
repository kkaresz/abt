import logging

logger = logging.getLogger(__name__)


class Logger(object):
	task = None
	state = None

	@classmethod
	def init_logger(cls):
		logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)

	@classmethod
	def set_task(cls, task):
		cls.task = task

	@classmethod
	def set_state(cls, state):
		cls.state = state

	@classmethod
	def info(cls, msg=""):
		message = "[%s::%s] %s" % (cls.task, cls.state, msg)
		logger.info(message)
			
	@classmethod
	def error(cls, msg=""):
		message = "[%s::%s] %s" % (cls.task, cls.state, msg)
		logger.error(message)
