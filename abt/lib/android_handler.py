from abt.lib.Config import Config
import os
import sys
from abt.lib.cmd_helper import run_shell_command
from abt.lib.logger import Logger

android_handler = None


class AndroidHandler():

	JAVA_HOME = None
	ANDROID_HOME = None
	BUILD_TOOL_VERSION = None

	def __init__(self):
		self.load_config()

	def load_config(self):
		self.ANDROID_HOME = os.environ.get('ANDROID_HOME')
		self.JAVA_HOME = os.environ.get('JAVA_HOME')
		self.set_build_tool_version()

	def build_tool_exists(self, path):
		return os.path.exists("%s/build-tools/%s" % (self.ANDROID_HOME, self.BUILD_TOOL_VERSION))

	# check the SDK folder for available build tool versions 
	# if it is not set in the config
	def set_build_tool_version(self):
		self.BUILD_TOOL_VERSION = Config.get("android", "build_tool_version")

		if not self.BUILD_TOOL_VERSION or not self.build_tool_exists(self.BUILD_TOOL_VERSION):
			result = []
			for folder in os.listdir("%s/build-tools" % self.ANDROID_HOME):
				result.append(os.path.join(folder))

			if len(result) > 0:
				self.BUILD_TOOL_VERSION = result[0]

		Logger.info("Android build-tool version is set to: %s" % self.BUILD_TOOL_VERSION)

	# list available android targets
	def list_targets(self):
		self.load_config()
		retcode, target_list = run_shell_command([
			"%s/tools/android" % self.ANDROID_HOME,
			"list",
			"target",
			"-c"
		])
		return target_list

	# create android project in the specified folder
	def create_project(self, path):
		self.load_config()

		name = Config.get("project", "name")
		target = Config.get("project", "target")
		package = Config.get("project", "package")
		activity = Config.get("project", "activity")

		retcode, output = run_shell_command([
			"%s/tools/android" % self.ANDROID_HOME, 
			"create", 
			"project",
			"--name", "%s" % name,
			"--target", "%s" % target,
			"--path", "%s" % path,
			"--package", "%s" % package,
			"--activity", "%s" % activity
		])
		return retcode

	def create_output_dirs(self):
		cmd_arguments = [
			"mkdir",
			"-p",
			"bin/res",
			"bin/classes",
			"bin/dexedLibs",
			"gen"
		]
		retcode, output = run_shell_command(cmd_arguments)
		return retcode

	def create_r_java(self):
		cmd_arguments = [
			"%s/build-tools/%s/aapt" % (self.ANDROID_HOME, self.BUILD_TOOL_VERSION),
			"package",
			"-v",
			"-f",
			"-m",
			"-0", "apk",
			"-M", "AndroidManifest.xml",
			"-S", "bin/res",
			"-S", "res",
			"-I", "%s/platforms/%s/android.jar" % (self.ANDROID_HOME, Config.get("project", "target")),
			"-J", "gen",
			"--generate-dependencies"
		]
		retcode, output = run_shell_command(cmd_arguments)
		return retcode

	def compile(self):
		cmd_arguments = [
			"%s/bin/javac" % self.JAVA_HOME,
			"-source", "1.7",
			"-target", "1.7",
			"-verbose",
			"-g",
			"-d", "bin/classes",
			"-classpath", "%s/platforms/%s/android.jar:bin/classes" % (self.ANDROID_HOME, Config.get("project", "target")),
			"-sourcepath", "src:gen",
			"-bootclasspath", "%s/platforms/android-7/android.jar" % self.ANDROID_HOME,
			"-encoding", "UTF-8"
		]

		# collect all .java files from the project directory
		for root, dirs, files in os.walk("./"):
			print root, dirs, files
			for f in files:
				if f.endswith(".java"):
					cmd_arguments.append(os.path.join(root, f))
		
		retcode, output = run_shell_command(cmd_arguments)
		return retcode

	# run the 3 necessary step to compile the project
	def compile_project(self):
		self.load_config()
		# create the necessary folders
		ret = self.create_output_dirs()
		if ret > 0:
			return ret

		# create R.java
		ret = self.create_r_java()
		if ret > 0:
			return ret

		# compile the code
		ret = self.compile()
		if ret > 0:
			return ret

		return 0

	def create_dex_file(self):
		cmd_arguments = [
			"%s/build-tools/%s/dx" % (self.ANDROID_HOME, self.BUILD_TOOL_VERSION),
			"--dex",
			"--verbose",
			"--output", "bin/classes.dex", "bin/classes", "bin/dexedLibs"
		]
		retcode, output = run_shell_command(cmd_arguments)
		return retcode
			
	def package_to_apk(self):
		cmd_arguments = [
			"%s/build-tools/%s/aapt" % (self.ANDROID_HOME, self.BUILD_TOOL_VERSION),
			"package",
			"-v",
			"-f",
			"-M", "AndroidManifest.xml",
			"-S", "bin/res",
			"-S", "res",
			"-I", "%s/platforms/%s/android.jar" % (self.ANDROID_HOME, Config.get("project", "target")),
			"-F", "bin/%s.unsigned.apk" % Config.get("project", "name"),
			"bin"
		]
		retcode, output = run_shell_command(cmd_arguments)
		return retcode

	def sign_apk(self):
		cmd_arguments = [
			"%s/bin/jarsigner" % self.JAVA_HOME,
			"-verbose",
			"-keystore", Config.get("android", "keystore_path"),
			"-storepass", Config.get("android", "keystore_storepass"),
			"-keypass", Config.get("android", "keystore_keypass"),
			"-signedjar", "bin/%s.signed.apk" % Config.get("project", "name"),
			"bin/%s.unsigned.apk" % Config.get("project", "name"),
			Config.get("android", "keystore_alias")
		]
		retcode, output = run_shell_command(cmd_arguments)
		return retcode

	def zip_align_apk(self):
		cmd_arguments = [
			"%s/build-tools/%s/zipalign" % (self.ANDROID_HOME, self.BUILD_TOOL_VERSION),
			"-v",
			"-f", "4",
			"bin/%s.signed.apk" % Config.get("project", "name"),
			"bin/%s-release.apk" % Config.get("project", "name")
		]
		retcode, output = run_shell_command(cmd_arguments)
		return retcode

	# run the 4 necessary steps for packaging the application
	def package_project(self):
		self.load_config()
		
		# create DEX file
		ret = self.create_dex_file()
		if ret > 0:
			return ret
		
		# package into an APK
		ret = self.package_to_apk()
		if ret > 0:
			return ret

		# sign the APK
		ret = self.sign_apk()
		if ret > 0:
			return ret
		
		# zip align the APK
		ret = self.zip_align_apk()
		if ret > 0:
			return ret

		return 0

	def launch_project(self, device=None):
		self.load_config()
		# launch app on a running emulator
		if device:
			cmd_arguments = [
				"%s/platform-tools/adb" % self.ANDROID_HOME,
				"-s", device, 
				"install",
				"bin/%s-release.apk" % Config.get("project", "name")
			]
		else: # launch app on a specified emulator
			cmd_arguments = [
				"%s/platform-tools/adb" % self.ANDROID_HOME,
				"-e", 
				"install",
				"bin/%s-release.apk" % Config.get("project", "name")
			]
		retcode, output = run_shell_command(cmd_arguments)
		return retcode
		
	# list running devices
	def get_emulators(self):
		self.load_config()
		cmd_arguments = [
			"%s/platform-tools/adb" % self.ANDROID_HOME,
			"devices"
		]
		retcode, output = run_shell_command(cmd_arguments)
		return output


def get_android_handler():
	global android_handler
	if android_handler is None:
		android_handler = AndroidHandler()
	return android_handler


def check_target(target):
	android = get_android_handler()
	targets = android.list_targets()
	if targets and target in targets:
		return True
	return False


def is_android_home_set():
	android = get_android_handler()
	android_home = android.ANDROID_HOME
	if not android_home or not os.path.exists(android_home):
		return False
	return True


def is_java_home_set():
	android = get_android_handler()
	java_home = android.JAVA_HOME
	if not java_home or not os.path.exists(java_home):
		return False
	return True


def create_android_project(path):
	android = get_android_handler()
	ret = android.create_project(path)
	if ret > 0:
		return False
	return True


def compile_android_project():
	android = get_android_handler()
	ret = android.compile_project()
	if ret > 0:
		return False
	return True


def package_android_project():
	android = get_android_handler()
	ret = android.package_project()
	if ret > 0:
		return False
	return True


def launch_android_project(device):
	android = get_android_handler()
	ret = android.launch_project(device)
	if ret > 0:
		return False
	return True


def get_running_emulators():
	android = get_android_handler()
	return android.get_emulators()
