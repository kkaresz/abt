from abt.lib.Config import Config
from abt.tasks.base import BaseTask
from abt.lib import android_handler
from abt.lib.cmd_helper import run_shell_command
from abt.lib.logger import Logger

import os
import sys
import shutil


class LaunchTask(BaseTask):

	def __init__(self):
		super(LaunchTask, self).__init__(self.TASK_LAUNCH)

	# validate the command line arguments that were passed to this task
	def check_arguments(self, args):
		super(LaunchTask, self).check_arguments(args)
		if args.device:
			running_devices = android_handler.get_running_emulators()
			if args.device not in running_devices:
				Logger.error("Could not find '%s' in running devices." % args.device)
				self.fail(args)
	
	# validate the configuration file and env vars
	def check_config(self, args):
		super(LaunchTask, self).check_config(args)
		if not Config.load_config():
			Logger.error("Could not load config file 'abt.config'.")
			self.fail(args)

		if not android_handler.is_android_home_set():
			Logger.error("Android home is not set properly.")
			self.fail(args)

		if not os.path.isfile("bin/%s-release.apk" % Config.get("project", "name")):
			Logger.error("Android package could not be found.")
			self.fail(args)				

	# run 'before' task command if it is defines
	def pre_execute(self, args):
		super(LaunchTask, self).pre_execute(args)
		before_task = Config.get(self.TASK_LAUNCH, self.PRE_EXECUTE_KEYWORD)
		if before_task:
			Logger.info("running before task: %s" % before_task)
			run_shell_command(before_task, True)

	# main function of the task
	def execute(self, args):
		super(LaunchTask, self).execute(args)
		if not android_handler.launch_android_project(args.device):
			self.fail(args)

	# run 'after' task command if it is defines
	def post_execute(self, args):
		super(LaunchTask, self).post_execute(args)
		after_task = Config.get(self.TASK_LAUNCH, self.POST_EXECUTE_KEYWORD)
		if after_task:
			Logger.info("running after task: %s" % after_task)
			run_shell_command(after_task, True)

	# exit on failure
	def fail(self, args):
		super(LaunchTask, self).fail(args)
		Logger.error("Could not launch project. Exiting.")
		sys.exit(1)

	# run all states of the task from check_arguments to post_execute
	def run(self, args):
		super(LaunchTask, self).run(args)
