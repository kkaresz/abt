import unittest

from mock import patch

from abt.lib.tests.unit_test_case_base import UnitTestCaseBase

from abt.tasks.create import CreateTask
from abt.lib.logger import Logger

from test.test_support import EnvironmentVarGuard

class Arguments(object):
    pass

class CreateTestCase(UnitTestCaseBase):

	log_info_patcher = None
	mock_log_info = None

	log_error_patcher = None
	mock_log_error = None

	fail_patcher = None
	mock_fail = None

	shell_command_patcher = None
	mock_shell = None

	test_args = Arguments()

	def setUp(self):
		super(CreateTestCase, self).setUp()
		self.log_info_patcher = patch('abt.lib.logger.Logger.info')
		self.mock_log_info = self.log_info_patcher.start()
		self.addCleanup(self.log_info_patcher.stop)

		self.log_error_patcher = patch('abt.lib.logger.Logger.error')
		self.mock_log_error = self.log_error_patcher.start()
		self.addCleanup(self.log_error_patcher.stop)

		self.fail_patcher = patch('abt.tasks.create.CreateTask.fail')
		self.mock_fail = self.fail_patcher.start()
		self.addCleanup(self.fail_patcher.stop)
		
		self.shell_command_patcher = patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
		self.mock_shell = self.shell_command_patcher.start()
		self.addCleanup(self.shell_command_patcher.stop)


	def tearDown(self):
		super(CreateTestCase, self).tearDown()

	@patch('abt.tasks.create.android_handler.is_android_home_set', return_value=False)
	@patch('abt.tasks.create.android_handler.is_java_home_set', return_value=True)
	@patch('abt.tasks.create.android_handler.AndroidHandler.build_tool_exists', return_value=True)
	def test_check_config_fails_if_android_home_is_not_set(self, mock_build_tool_exists, mock_java_home, mock_android_home):
		task = CreateTask()
		task.check_config(self.test_args)
		self.mock_fail.assert_called()

	@patch('abt.tasks.create.android_handler.is_android_home_set', return_value=True)
	@patch('abt.tasks.create.android_handler.is_java_home_set', return_value=False)
	@patch('abt.tasks.create.android_handler.AndroidHandler.build_tool_exists', return_value=True)
	def test_check_config_fails_if_java_home_is_not_set(self, mock_build_tool_exists, mock_java_home, mock_android_home):
		task = CreateTask()
		task.check_config(self.test_args)
		self.mock_fail.assert_called()

	@patch('abt.tasks.create.android_handler.AndroidHandler.create_project')
	def test_execute_calls_android_handler_create_project_once(self, mock_create_project):
		task = CreateTask()
		self.test_args.path = "testPath"
		task.execute(self.test_args)
		mock_create_project.assert_called_once()
		
	@patch('abt.lib.Config.Config.write_to_file')
	def test_post_execute_writes_config_file_to_disk(self, mock_config):
		task = CreateTask()
		task.post_execute(self.test_args)
		mock_config.assert_called_once()