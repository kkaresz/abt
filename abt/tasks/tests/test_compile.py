import unittest

from mock import patch, call

from abt.lib.tests.unit_test_case_base import UnitTestCaseBase

from abt.tasks.compile import CompileTask
from abt.lib.logger import Logger
from abt.lib.Config import Config

from test.test_support import EnvironmentVarGuard

class Arguments(object):
    pass

class CompileTestCase(UnitTestCaseBase):

	log_info_patcher = None
	mock_log_info = None

	log_error_patcher = None
	mock_log_error = None

	fail_patcher = None
	mock_fail = None

	shell_command_patcher = None
	mock_shell = None

	test_args = Arguments()

	def setUp(self):
		super(CompileTestCase, self).setUp()
		self.log_info_patcher = patch('abt.lib.logger.Logger.info')
		self.mock_log_info = self.log_info_patcher.start()
		self.addCleanup(self.log_info_patcher.stop)

		self.log_error_patcher = patch('abt.lib.logger.Logger.error')
		self.mock_log_error = self.log_error_patcher.start()
		self.addCleanup(self.log_error_patcher.stop)

		self.fail_patcher = patch('abt.tasks.compile.CompileTask.fail')
		self.mock_fail = self.fail_patcher.start()
		self.addCleanup(self.fail_patcher.stop)
		
		self.shell_command_patcher = patch('abt.lib.android_handler.run_shell_command', return_value=[0, "test output"])
		self.mock_shell = self.shell_command_patcher.start()
		self.addCleanup(self.shell_command_patcher.stop)


	def tearDown(self):
		super(CompileTestCase, self).tearDown()
		Config.clear_config()

	@patch('abt.tasks.compile.android_handler.is_android_home_set', return_value=False)
	@patch('abt.tasks.compile.android_handler.AndroidHandler.build_tool_exists', return_value=True)
	def test_check_config_fails_if_android_home_is_not_set(self, mock_build_tool_exists, mock_android_home):
		task = CompileTask()
		task.check_config(self.test_args)
		self.mock_fail.assert_called()

	@patch('abt.tasks.compile.android_handler.is_java_home_set', return_value=False)
	@patch('abt.tasks.compile.android_handler.AndroidHandler.build_tool_exists', return_value=True)
	def test_check_config_fails_if_java_home_is_not_set(self, mock_build_tool_exists, mock_java_home):
		task = CompileTask()
		task.check_config(self.test_args)
		self.mock_fail.assert_called()

	@patch('abt.lib.Config.Config.load_config', return_value=False)
	@patch('abt.tasks.compile.android_handler.AndroidHandler.build_tool_exists', return_value=True)
	def test_check_config_fails_if_config_file_cannot_be_loaded(self, mock_build_tool_exists, mock_config):
		task = CompileTask()
		task.check_config(self.test_args)
		self.mock_fail.assert_called()

	@patch('abt.tasks.compile.run_shell_command', return_value=[0, "test output"])
	@patch('abt.tasks.compile.android_handler.AndroidHandler.build_tool_exists', return_value=True)
	def test_pre_execute_runs_shell_if_before_is_set_in_config(self, mock_build_tool_exists, mock_compile_shell):
		Config.inject_config('[task_compile]\nbefore = echo Test')
		task = CompileTask()
		task.pre_execute(self.test_args)

		mocked_call_list = mock_compile_shell.call_args_list[0]
		expected_call_list = call(unicode("echo Test"), True)

		self.assertEqual(mocked_call_list, expected_call_list)

	@patch('abt.tasks.compile.android_handler.AndroidHandler.compile_project')
	def test_execute_calls_android_handler_compile_project_once(self, mock_compile_project):
		task = CompileTask()
		task.execute(self.test_args)
		mock_compile_project.assert_called_once()

	@patch('abt.tasks.compile.run_shell_command', return_value=[0, "test output"])
	@patch('abt.tasks.compile.android_handler.AndroidHandler.build_tool_exists', return_value=True)
	def test_post_execute_runs_shell_if_after_is_set_in_config(self, mock_build_tool_exists, mock_compile_shell):
		Config.inject_config('[task_compile]\nafter = echo Test')
		task = CompileTask()
		task.post_execute(self.test_args)

		mocked_call_list = mock_compile_shell.call_args_list[0]
		expected_call_list = call(unicode("echo Test"), True)

		self.assertEqual(mocked_call_list, expected_call_list)
