from abt.lib.Config import Config
from abt.tasks.base import BaseTask
from abt.lib import android_handler
from abt.lib.cmd_helper import run_shell_command
from abt.lib.logger import Logger

import os
import sys
import shutil


class CompileTask(BaseTask):

	def __init__(self):
		super(CompileTask, self).__init__(self.TASK_COMPILE)

	# validate the command line arguments that were passed to this task
	def check_arguments(self, args):
		super(CompileTask, self).check_arguments(args)
		Logger.info("No arguments needed for this task.")
	
	# validate the configuration file and env vars
	def check_config(self, args):
		super(CompileTask, self).check_config(args)
		if not Config.load_config():
			Logger.error("Could not load config file 'abt.config'.")
			self.fail(args)

		if not android_handler.is_android_home_set():
			Logger.error("Android home is not set properly.")
			self.fail(args)

		if not android_handler.is_java_home_set():
			Logger.error("Java home is not set properly.")
			self.fail(args)		

	# run 'before' task command if it is defines
	def pre_execute(self, args):
		super(CompileTask, self).pre_execute(args)
		before_task = Config.get(self.TASK_COMPILE, self.PRE_EXECUTE_KEYWORD)
		if before_task:
			Logger.info("running before task: %s" % before_task)
			run_shell_command(before_task, True)

	# main function of the task
	def execute(self, args):
		super(CompileTask, self).execute(args)
		if not android_handler.compile_android_project():
			self.fail(args)

	# run 'after' task command if it is defines
	def post_execute(self, args):
		super(CompileTask, self).post_execute(args)
		after_task = Config.get(self.TASK_COMPILE, self.POST_EXECUTE_KEYWORD)
		if after_task:
			Logger.info("running after task: %s" % after_task)
			run_shell_command(after_task, True)

	# exit on failure
	def fail(self, args):
		super(CompileTask, self).fail(args)
		Logger.error("Could not compile project. Exiting.")
		sys.exit(1)

	# run all states of the task from check_arguments to post_execute
	def run(self, args):
		super(CompileTask, self).run(args)
