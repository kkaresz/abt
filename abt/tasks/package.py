from abt.lib.Config import Config
from abt.tasks.base import BaseTask
from abt.lib import android_handler
from abt.lib.cmd_helper import run_shell_command
from abt.lib.logger import Logger

import os
import sys
import shutil


class PackageTask(BaseTask):

	def __init__(self):
		super(PackageTask, self).__init__(self.TASK_PACKAGE)

	def print_package_task_help(self):
		Logger.error("Missing keystore attribute.")
		Logger.error("You can set the keystore attributes in 'abt.config' under 'android' section e.g. 'keystore_path = ./MyKestore.keystore'")
		Logger.error("Or via a parameter e.g. 'abt package --keystore-path ./MyKestore.keystore'")
		Logger.error("Please run 'abt package -h' for details")

	# validate the command line arguments that were passed to this task
	def check_arguments(self, args):
		super(PackageTask, self).check_arguments(args)
		if not Config.load_config():
			Logger.error("Could not load config file 'abt.config'.")
			self.fail(args)
		
		if not Config.get("android", "keystore_path"):
			if not args.keystore_path:
				self.print_package_task_help()
				self.fail(args)
			else:
				Config.set("android", "keystore_path", args.keystore_path)

		if not Config.get("android", "keystore_alias"):
			if not args.keystore_alias:
				self.print_package_task_help()
				self.fail(args)
			else:
				Config.set("android", "keystore_alias", args.keystore_alias)				

		if not Config.get("android", "keystore_storepass"):
			if not args.keystore_storepass:
				self.print_package_task_help()
				self.fail(args)
			else:
				Config.set("android", "keystore_storepass", args.keystore_storepass)

		if not Config.get("android", "keystore_keypass"):
			if not args.keystore_keypass:
				self.print_package_help()
				self.fail(args)
			else:
				Config.set("android", "keystore_keypass", args.keystore_keypass)
	
	# validate the configuration file and env vars
	def check_config(self, args):
		super(PackageTask, self).check_config(args)
		if not Config.load_config():
			Logger.error("Could not load config file 'abt.config'.")
			self.fail(args)

		if not android_handler.is_android_home_set():
			Logger.error("Android home is not set properly.")
			self.fail(args)

		if not android_handler.is_java_home_set():
			Logger.error("Java home is not set properly.")
			self.fail(args)		

	# run 'before' task command if it is defines
	def pre_execute(self, args):
		super(PackageTask, self).pre_execute(args)
		before_task = Config.get(self.TASK_PACKAGE, self.PRE_EXECUTE_KEYWORD)
		if before_task:
			Logger.info("running before task: %s" % before_task)
			run_shell_command(before_task, True)

	# main function of the task
	def execute(self, args):
		super(PackageTask, self).execute(args)
		if not android_handler.package_android_project():
			self.fail(args)

	# run 'after' task command if it is defines
	def post_execute(self, args):
		super(PackageTask, self).post_execute(args)
		after_task = Config.get(self.TASK_PACKAGE, self.POST_EXECUTE_KEYWORD)
		if after_task:
			Logger.info("running after task: %s" % after_task)
			run_shell_command(after_task, True)

	# exit on failure
	def fail(self, args):
		super(PackageTask, self).fail(args)
		Logger.error("Could not package project. Exiting.")
		sys.exit(1)

	# run all states of the task from check_arguments to post_execute
	def run(self, args):
		super(PackageTask, self).run(args)
