from abt.lib.Config import Config
from abt.tasks.base import BaseTask
from abt.lib import android_handler
from abt.lib.logger import Logger

import os
import sys
import shutil

MAX_PROJECT_NAME_LENGTH = 50
MAX_PATH_LENGTH = 255


class CreateTask(BaseTask):

	def __init__(self):
		super(CreateTask, self).__init__(self.TASK_CREATE)

	# validate the command line arguments that were passed to this task
	def check_arguments(self, args):
		super(CreateTask, self).check_arguments(args)
		
		name = args.name
		target = args.target
		path = args.path
		package = args.package
		activity = args.activity

		if len(name) > MAX_PROJECT_NAME_LENGTH:
			Logger.error("Project name %s too long." % name)
			self.fail()	

		if len(path) > MAX_PATH_LENGTH:
			Logger.error("Path %s too long." % path)
			self.fail()

		if os.path.exists(args.path):
			Logger.error("Path %s already exists." % args.path)
			self.fail(args)

		Config.create_config()

		Config.add_section("project")
		Config.set("project", "name", name)
		Config.set("project", "target", target)
		Config.set("project", "package", package)
		Config.set("project", "activity", activity)

	# validate the configuration file and env vars
	def check_config(self, args):
		super(CreateTask, self).check_config(args)

		if not android_handler.is_android_home_set():
			Logger.error("Android home is not set properly.")
			self.fail(args)

		if not android_handler.is_java_home_set():
			Logger.error("Java home is not set properly.")
			self.fail(args)

		if not android_handler.check_target(Config.get("project", "target")):
			Logger.error("Could not find Android target %s" % Config.get("project", "target"))
			self.fail(args)

		Config.print_config()

	# run 'before' task command if it is defines
	def pre_execute(self, args):
		super(CreateTask, self).pre_execute(args)
		Logger.info("Nothing to run before create.")

	# main function of the task
	def execute(self, args):
		super(CreateTask, self).execute(args)
		if not android_handler.create_android_project(args.path):
			self.fail(args)

	# run 'after' task command if it is defines
	def post_execute(self, args):
		super(CreateTask, self).post_execute(args)
		try: 
			Config.write_to_file("%s/%s" % (args.path, Config.CONFIG_FILE))
		except:
			self.fail(args)

	# exit on failure
	def fail(self, args):
		super(CreateTask, self).fail(args)
		Logger.error("Could not create project. Exiting.")
		sys.exit(1)

	# run all states of the task from check_arguments to post_execute
	def run(self, args):
		super(CreateTask, self).run(args)
