from datetime import datetime
from abt.lib.logger import Logger


class BaseTask(object):
	task_name = None
	state = None

	TASK_CREATE = "task_create"
	TASK_COMPILE = "task_compile"
	TASK_PACKAGE = "task_package"
	TASK_LAUNCH = "task_launch"

	STATE_CHECK_ARGUMENTS = "check_arguments"
	STATE_CHECK_CONFIG = "check_config"
	STATE_PRE_EXECUTE = "pre_execute"
	STATE_EXECUTE = "execute"
	STATE_POST_EXECUTE = "post_execute"
	STATE_FAIL = "fail"
	STATE_RUN = "run"

	PRE_EXECUTE_KEYWORD = "before"
	POST_EXECUTE_KEYWORD = "after"

	def __init__(self, task_name):
		self.task_name = task_name
		Logger.set_task(task_name)

	def set_state(self, state):
		self.state = state
		Logger.set_state(state)

	def log_started(self):
		time = datetime.now()
		Logger.info("Started at %s" % time.strftime('%Y-%m-%d %H:%M:%S'))

	def check_arguments(self, args):
		self.set_state(self.STATE_CHECK_ARGUMENTS)
		self.log_started()
		
	def check_config(self, args):
		self.set_state(self.STATE_CHECK_CONFIG)
		self.log_started()

	def pre_execute(self, args):
		self.set_state(self.STATE_PRE_EXECUTE)
		self.log_started()

	def execute(self, args):
		self.set_state(self.STATE_EXECUTE)
		self.log_started()

	def post_execute(self, args):
		self.set_state(self.STATE_POST_EXECUTE)
		self.log_started()

	def fail(self, args):
		self.set_state(self.STATE_FAIL)
		self.log_started()

	def run(self, args):
		self.set_state(self.STATE_RUN)
		self.check_arguments(args)
		self.check_config(args)
		self.pre_execute(args)
		self.execute(args)
		self.post_execute(args)
